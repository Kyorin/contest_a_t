# Notre projet de contest : contest_a_t 
##contest-a-t
Contest-a-t est un packtage comprenant du Python et des documents docx et txt se trouvant sur le serveur Gitlab.

###Installation

#### Pré-requis
Pour exécuter le projet contest-a-t, il faut pouvoir :
- effectuer des requêtes Python
- posséder un éditeur de texte de type Notepad ++ ou un IDE du type PyCharm
- posséder word ou accéder à microsoft office 365
- posséder un compte Gitlab

####Installer le projet
- Récupérer le projet via Gitlab
- Ouvrir une invite de commande 
- Copier l'url du projet donné sur le Gitlab
- Exécuter un ``git clone`` avec ajout de l'url

#####Après avoir récupérer le dossier, il suffit d'ouvrir les différents documents avec les différents supports nécessaires à cet effet.

####Documentation
La documentation complète de ce projet se trouve dans le README.md ci présent.

## Contenu du projet
### Algorithme du projet
######Le fichier se dénomme algo#sapin.txt
Celui-ci comprend les algorithmes :
######Niveau 1
######Niveau 2
######Niveau 3
######Niveau 4
######Niveau 5

### Le code Python 
####Niveau 1
#####Sous le nom sapin_lvl1.py
```python 
def floor(nb_lines, nb_star, nb_caracterMax):
    for i in range(nb_lines):
        nb = nb_star * (i + 1)
        print(" " * (nb_caracterMax - nb) + "*" * (nb * 2 - 1))

def tree(nb_floors, nb_lines_by_floors):
    nb_max_stars = nb_floors * nb_lines_by_floors
    for i in range(nb_floors):
        floor(nb_lines_by_floors, i + 1, nb_max_stars)

tree(3, 4)
```
####Niveau 2
#####Sous le nom sapin_lvl2.py
```python
def floor(nb_lines, nb_star, nb_caracterMax):
    for i in range(nb_lines):
        nb = nb_star* (i + 1)
        print(" " * (nb_caracterMax - nb) + "*" * (nb * 2 - 1))

def trunck(nb_lines, nb_stars, nb_caracterMax):
    for i in range(nb_lines):
        print(" " * (nb_caracterMax - nb_stars) + "*" * (1 + 2 *nb_stars))

def tree(nb_floors, nb_lines_by_floors):
    nb_max_stars = nb_floors * nb_lines_by_floors
    for i in range(nb_floors):
        floor(nb_lines_by_floors, i + 1, nb_max_stars)
    trunck(3, 2, nb_max_stars-1)

tree(3, 4)
```
####Niveau 3
#####Sous le nom sapin_lvl3.py
```python
def floor(nb_lines, nb_star, nb_caracterMax):
    for i in range(nb_lines):
        nb = nb_star * (i + 1)
        print(" " * (nb_caracterMax - nb) + "*" * (nb * 2 - 1))

def trunck(nb_lines, nb_stars, nb_garlands):
    for i in range(nb_lines):
        if(i == 0): garland_caracter = "|"
        elif(i == 1): garland_caracter = "0"
        else: garland_caracter = " "
        line = " "
        for j in range(nb_garlands):
            line = line + garland_caracter + " "

        print(line + "*" * (1 + 2 *nb_stars) + line)

def tree(nb_floors, nb_lines_by_floors):
    nb_max_stars = nb_floors * nb_lines_by_floors
    for i in range(nb_floors):
        floor(nb_lines_by_floors, i + 1, nb_max_stars)
    trunck(3, 2, 4)

tree(3, 4)
```
####Niveau 4
#####Sous le nom sapin_lvl4.py
```python
def floor(nb_lines, nb_star, nb_caracterMax):
    for i in range(nb_lines):
        nb = nb_star * (i + 1)
        if(i == 0 and nb_star > 1):
            spaces = " " * (nb_caracterMax - nb_lines * (nb_star - 1))
            spaces_ball = " " * (nb_star - 1) * (nb_star - 1)
            print(spaces + "0" + spaces_ball + "*" * (nb * 2 - 1) + spaces_ball + "0")
        else:
            print(" " * (nb_caracterMax - nb) + "*" * (nb * 2 - 1))

def trunck(nb_lines, nb_stars, nb_garlands):
    for i in range(nb_lines):
        if(i == 0): garland_caracter = "|"
        elif(i == 1): garland_caracter = "0"
        else: garland_caracter = " "
        line = " "
        for j in range(nb_garlands):
            line = line + garland_caracter + " "

        print(line + "*" * (1 + 2 *nb_stars) + line)

def tree(nb_floors, nb_lines_by_floors):
    nb_max_stars = nb_floors * nb_lines_by_floors
    for i in range(nb_floors):
        floor(nb_lines_by_floors, i + 1, nb_max_stars)
    trunck(3, 2, 4)

tree(3, 4)
```
####Niveau 5
#####Sous le nom sapin_lvl5.py
```python
def star(nb_lines, nb_column, nb_caracterMax):
    for i in range(nb_lines):
        star = "*"
        if(i == 0 or i == 6):
            space_before = ""
            space_after = "    "
            if(i == 0): center = "*"
            elif(i == 4): center = "|"
        elif (i == 1 or i == 5):
            space_before = "  "
            space_after = "  "
            if(i == 1): center = "*"
            elif(i == 5): center = "|"
        elif (i == 2 or i == 4):
            space_before = "  "
            space_after = "  "
            center = "*"
            star = " "
        elif (i == 3):
            space_before = "* * *"
            space_after = ""
            center = " "
            star = ""

        print((nb_caracterMax - nb_column) * " " + space_before + star + space_after + center + space_after + star + space_before)

def floor(nb_lines, nb_star, nb_caracterMax):
    for i in range(nb_lines):
        nb = nb_star * (i + 1)
        if(i == 0 and nb_star > 1):
            spaces = " " * (nb_caracterMax - nb_lines * (nb_star - 1))
            spaces_ball = " " * (nb_star - 1) * (nb_star - 1)
            print(spaces + "0" + spaces_ball + "*" * (nb * 2 - 1) + spaces_ball + "0")
        else:
            print(" " * (nb_caracterMax - nb) + "*" * (nb * 2 - 1))

def trunck(nb_lines, nb_stars, nb_garlands):
    for i in range(nb_lines):
        if(i == 0): garland_caracter = "|"
        elif(i == 1): garland_caracter = "0"
        else: garlande_caracter = " "
        line = " "
        for j in range(nb_garlands):
            line = line + garland_caracter + " "

        print(line + "*" * (1 + 2 *nb_stars) + line)

def tree(nb_floors, nb_lines_by_floors):
    nb_max_stars = nb_floors * nb_lines_by_floors
    star(7, 5, nb_max_stars - 1)
    for i in range(nb_floors):
        floor(nb_lines_by_floors, i + 1, nb_max_stars)
    trunck(3, 2, 4)

tree(3, 4)
```
####Le bonus 1
#####Sous le nom sapin_bonus1.py
```python
def star(nb_lines, nb_column, nb_caracterMax):
    for i in range(nb_lines):
        star = "*"
        if(i == 0 or i == 6):
            space_before = ""
            space_after = "    "
            if(i == 0): center = "*"
            elif(i == 4): center = "|"
        elif (i == 1 or i == 5):
            space_before = "  "
            space_after = "  "
            if(i == 1): center = "*"
            elif(i == 5): center = "|"
        elif (i == 2 or i == 4):
            space_before = "  "
            space_after = "  "
            center = "*"
            star = " "
        elif (i == 3):
            space_before = "* * *"
            space_after = ""
            center = " "
            star = ""

        print((nb_caracterMax - nb_column) * " " + space_before + star + space_after + center + space_after + star + space_before)

def floor(nb_lines, nb_star, nb_caracterMax):
    for i in range(nb_lines):
        nb = nb_star * (i + 1)
        if(i == 0 and nb_star > 1):
            spaces = " " * (nb_caracterMax - nb_lines * (nb_star - 1))
            spaces_ball = " " * (((nb_star - 1) * nb_lines - 1) - nb)
            print(spaces + "0" + spaces_ball + "*" * (nb * 2 - 1) + spaces_ball + "0")
        else:
            print(" " * (nb_caracterMax - nb) + "*" * (nb * 2 - 1))

def trunck(nb_lines, nb_stars, nb_max_stars):
    for i in range(nb_lines):
        if(i == 0): garland_caracter = "|"
        elif(i == 1): garland_caracter = "0"
        else: garland_caracter = " "
        line = ""
        for j in range(nb_max_stars - 1 - nb_stars):
            if (j % 2) == 0:
                line = line + " "
            else:
                line = line + garland_caracter

        print(line + "*" * (1 + 2 *nb_stars) + line)

def tree(nb_floors, nb_lines_by_floors):
    nb_max_stars = nb_floors * nb_lines_by_floors
    star(7, 5, nb_max_stars - 1)
    for i in range(nb_floors):
        floor(nb_lines_by_floors, i + 1, nb_max_stars)
    trunck(3, 2, nb_max_stars)

tree(4, 4)
```
####Le bonus 2
#####Sous le nom sapin_bonus2.py
```python
def star(nb_lines, nb_column, nb_caracterMax, spaces_between_tree, nb_trees):
    for i in range(nb_lines):
        etoile = "*"
        if(i == 0 or i == 6):
            space_before = ""
            space_after = "    "
            if(i == 0): center = "*"
            elif(i == 4): center = "|"
        elif (i == 1 or i == 5):
            space_before = "  "
            space_after = "  "
            if(i == 1): center = "*"
            elif(i == 5): center = "|"
        elif (i == 2 or i == 4):
            space_before = "  "
            space_after = "  "
            center = "*"
            star = " "
        elif (i == 3):
            space_before = "* * *"
            space_after = ""
            center = " "
            star = ""

        line = (nb_caracterMax - nb_column) * " " + space_before + star + space_after + center + space_after + star + space_before + (nb_caracterMax - nb_column) * " "

        print((line + spaces_between_tree) *2)

def floor(nb_lines, nb_star, nb_caracterMax, spaces_between_tree, nb_trees):
    for i in range(nb_lines):
        nb = nb_star * (i + 1)
        if(i == 0 and nb_star > 1):
            spaces = " " * (nb_caracterMax - nb_lines * (nb_star - 1))
            spaces_ball = " " * (((nb_star - 1) * nb_lines - 1) - nb)
            line = spaces + "0" + spaces_ball + "*" * (nb * 2 - 1) + spaces_ball + "0" + spaces
        else:
            spaces = " " * (nb_caracterMax - nb);
            line = spaces + "*" * (nb * 2 - 1) + spaces
        print((line + spaces_between_tree) * nb_trees)

def trunck(nb_lines, nb_stars, nb_max_stars, spaces_between_tree, nb_trees):
    for i in range(nb_lines):
        if(i == 0): garland_caracter = "|"
        elif(i == 1): garland_caracter = "0"
        else: garland_caracter = " "
        line = ""
        for j in range(nb_max_stars - 1 - nb_stars):
            if (j % 2) == 0:
                line = line + " "
            else:
                line = line + garland_caracter

        line = line + "*" * (1 + 2 *nb_stars) + line
        print((line + spaces_between_tree) * nb_trees)

def tree(nb_floors, nb_lines_by_floors, spaces_between_tree, nb_trees):
    nb_max_estars = nb_floors * nb_lines_by_floors
    star(7, 5, nb_max_stars - 1, spaces_by_tree, nb_trees)
    for i in range(nb_floors):
        floor(nb_lines_by_floors, i + 1, nb_max_stars, spaces_between_tree, nb_trees)
    trunck(3, 2, nb_max_stars, spaces_by_tree, nb_trees)


def forest(nb_trees, spaces_between_tree):
    tree(4, 4, spaces_between_tree * " ", nb_trees)
nb_tree = 2
nb_tree = 3
forest(nb_tree, nb_space)
```

### La documentation de l'installation d'un Git sur une VM locale protégée par un routeur PFSense
Nous avons fait le choix de choisir Gitlab et la VMWare.
######Le fichier se dénomme cp_procedure.docx.
Celui-ci explicite comment télécharger et appliquer les différents logiciels ou supports pour exécuter ce projet.
### La documentation du Bonus 1
#### Mettre en place un Git dans une VM cloud Azure à partir de portal.azure.com
######Le fichier se dénomme Bonus Azure.docx.
#### A partir de domicile d'un membre du binôme parvenir à accéder à son git via la box
######Le fichier se dénomme Bonus_git.docx .

## Répartition du travail
Pour ce projet, Télio et moi-même, nous nous sommes répartis les tâches de manière équitable afin que chacun puisse appréhender le sujet de manière globale.
Dans un premier temps, nous nous sommes concentrés sur la partie Computer Fundamentals puis nous nous sommes attelés à l'algorithme et au développement du code.
Une première version a été effectué cependant, elle ne permettait pas de répondre aux attentes globale du projet. Il a donc fallut le recommencer afin de remplir toutes les fonctionnalités demandées. 

## Contribution
Vous pouvez contribuer à ce projet de différentes manières :
- en écrivant dans la documentation, 
- en fixant les bugs, 
- en corrigeant les erreurs.